// MIT License
//
// Copyright (c) 2021-2023 Tobias Pfeiffer
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

use super::*;

pub fn run(
	reader: impl io::BufRead,
	mut writer: impl io::Write
) {
	let mut metrics = HashMap::new();

	for line in reader.lines() {
		let msg = match line.and_then(|line| serde_json::from_str(&line)
			.map_err(|e| io::Error::new(io::ErrorKind::InvalidData, e)))
		{
			Ok(v) => v,
			Err(e) => {
				eprintln!("error: failed to parse message: {e}");
				std::process::exit(1);
			}
		};

		let msg = match msg {
			clippy::Message::CompilerMessage(v) if v.message.spans.is_empty() => v,
			_ => continue
		};

		*metrics.entry((msg.message.level, msg.message.code.as_ref()
			.map_or_else(|| "unknown".to_string(), |v| v.code.clone())))
			.or_insert(0) += 1;
	}

	eprintln!("  \x1b[32;1mGenerating\x1b[0m OpenMetrics report");

	for ((level, code), value) in metrics {
		if let Err(e) = writeln!(&mut writer, "{level}{{code={code}}}: {value}") {
			eprintln!("error: failed to generate report: {e}");
			std::process::exit(1);
		}
	}
}