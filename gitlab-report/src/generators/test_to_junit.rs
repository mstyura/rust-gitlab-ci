// MIT License
//
// Copyright (c) 2021-2023 Tobias Pfeiffer
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

use super::*;

pub fn run(
	reader: impl io::BufRead,
	mut writer: impl std::fmt::Write
) {
	let mut suites = Vec::new();

	for line in reader.lines() {
		let msg = match line.and_then(|line| serde_json::from_str(&line)
			.map_err(|e| io::Error::new(io::ErrorKind::InvalidData, e)))
		{
			Ok(v) => v,
			Err(e) => {
				eprintln!("error: failed to parse message: {e}");
				std::process::exit(1);
			}
		};

		match msg {
			cargo::CargoMessage::Suite(cargo::CargoTestReportSuite::Started(v)) => {
				let now = std::time::UNIX_EPOCH.elapsed().unwrap().as_nanos();
				suites.push(junit::Testsuite {
					id:         suites.len(),
					name:       format!("cargo test #{}", suites.len()),
					timestamp:  chrono::NaiveDateTime::from_timestamp_opt(
						(now / 1_000_000_000) as _, (now % 1_000_000_000) as _)
						.expect("Invalid timestamp provided")
						.format("%Y-%m-%dT%H:%M:%S").to_string(),
					hostname:   "localhost".to_string(),
					tests:      v.test_count,
					testcases:  Some(Vec::new()),
					..Default::default()
				});
			}
			cargo::CargoMessage::Suite(cargo::CargoTestReportSuite::Ok(v) | cargo::CargoTestReportSuite::Failed(v)) => {
				let suite = suites.last_mut().unwrap();
				suite.failures = v.failed;
				suite.errors   = 0;
				suite.skipped  = v.ignored + v.filtered_out;
				suite.time     = v.exec_time;
			}
			cargo::CargoMessage::Test(cargo::CargoTestReportTest { name, event }) => {
				let now = std::time::UNIX_EPOCH.elapsed().unwrap().as_secs_f64();
				let suite = suites.last_mut().unwrap();
				let testcases = suite.testcases.as_mut().unwrap();
				let (module, name) = name.rsplit_once("::").unwrap_or(("", &name));

				match event {
					cargo::CargoTestReportTestEvent::Started => suite.testcases.as_mut().unwrap().push(junit::TestsuiteTestcase {
						status:    None,
						name:      name.to_string(),
						classname: module.to_string(),
						time:      now
					}),
					cargo::CargoTestReportTestEvent::Timeout => (),
					cargo::CargoTestReportTestEvent::Ignored => {
						let testcase = testcases.iter_mut()
							.find(|case| case.classname == module && case.name == name)
							.unwrap();

						testcase.time = now - testcase.time;
						testcase.status = Some(junit::TestsuiteTestcaseStatus::Skipped);
					},
					event @ cargo::CargoTestReportTestEvent::Ok(_) | event @ cargo::CargoTestReportTestEvent::Failed(_) => {
						let testcase = testcases.iter_mut()
							.find(|case| case.classname == module && case.name == name)
							.unwrap();

						testcase.time = now - testcase.time;
						testcase.status = match event {
							cargo::CargoTestReportTestEvent::Ok(_v) => None,
							cargo::CargoTestReportTestEvent::Failed(v) => Some(junit::TestsuiteTestcaseStatus::Failure {
								r#type:  "cargo test".to_string(),
								message: v.stdout.unwrap_or_default()
							}),
							_ => unreachable!()
						};
					}
				}
			}
			cargo::CargoMessage::Bench(_) => ()
		}
	}

	eprintln!("  \x1b[32;1mGenerating\x1b[0m JUnit report");

	if let Err(e) = writeln!(&mut writer, "<?xml version=\"1.0\" encoding=\"utf-8\"?>") {
		eprintln!("error: failed to generate report: {e:?}");
		std::process::exit(1);
	} else if let Err(e) = quick_xml::se::to_writer_with_root(writer, "testsuites", &junit::Report { suites }) {
		eprintln!("error: failed to generate report: {e:?}");
		std::process::exit(1);
	}
}